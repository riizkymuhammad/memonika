<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ADMIN AKADIN.ID</title>
    @extends('parts.style')
    <link rel="stylesheet" href="{{ asset('assets/admin/css/jquery.fileuploader.css') }}">
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
@extends('parts.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="margin-top: 0 !important">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2 mt-5">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Edit Undangan</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="">Home</a></li>
                    <li class="breadcrumb-item"><a href="">List Undangan</a></li>
                    <li class="breadcrumb-item active">Edit</li>
                    </ol>
                </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card card-body ">
                            <?php 
                            $data['w'] = $wedding;
                            $this->load->view('form_undangan',$data); ?>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h4 class="mb-3">Media <br>
                        <small style="font-size:11pt">Untuk menghapus gambar, silahkah klik tombol merah di setiap foto</small></h4><hr>
                        <p>Foto Sampul</p>
                        <div class="media-pic">
                            <img src="<?= $wedding['foto_sampul'] ?>" alt="" class="w-100">
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <p>Foto Pria</p>
                                <div class="media-pic">
                                    <img src="<?= $wedding['foto_pria'] ?>" alt="" class="w-100">
                                </div> 
                            </div>
                            <div class="col-md-6">
                                <p>Foto Wanita</p>
                                <div class="media-pic">
                                    <img src="<?= $wedding['foto_wanita'] ?>" alt="" class="w-100">
                                </div>   
                            </div>
                        </div>
                        <hr>
                        <h4>Galeri</h4>
                        <div class="row">
                        <?php foreach($this->db->get_where('invitation_gallery', array('invitation_id' => $wedding['invitation_id']))->result() as $img): ?>
                        <div class="col-md-6">
                            <div class="media-pic">
                                <a href="<?= base_url('gal/'.$img->id.'/del') ?>" onclick="return del_confirm()" /><i class="fas fa-trash-alt"></i></a>
                                <img src="<?= $img->filename ?>" alt="" class="w-100">
                            </div>
                        </div>  
                        <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->


    <!-- Main Footer -->
    <footer class="main-footer">
        <strong>Copyright &copy; <?= date('Y') ?> akadin.ID</strong>
        All rights reserved.
        <div class="float-right d-none d-sm-inline-block">
        Powered by <b>Gidicode Project</b>
        </div>
    </footer>
</div>
<!-- ./wrapper -->

<?php $this->load->view('parts/script') ?>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="<?= base_url('assets/admin/') ?>plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="<?= base_url('assets/admin/') ?>plugins/raphael/raphael.min.js"></script>
<script src="<?= base_url('assets/admin/') ?>plugins/jquery-mapael/jquery.mapael.min.js"></script>
<script src="<?= base_url('assets/admin/') ?>plugins/jquery-mapael/maps/usa_states.min.js"></script>
<!-- ChartJS -->
<script src="<?= base_url('assets/admin/') ?>plugins/chart.js/Chart.min.js"></script>

<!-- PAGE SCRIPTS -->
<script src="<?= base_url('assets/admin/') ?>js/pages/dashboard2.js"></script>
<script src="<?= base_url('assets/admin/') ?>js/jquery.fileuploader.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

<script>
    
    $( "#withAkad" ).on( "click", function() {
        $("#infoAkad").css('display','block');
    });

    $( "#noAkad" ).on( "click", function() {
        $("#infoAkad").css('display','none');
    });

    $( "#withGaleri" ).on( "click", function() {
        $("#addgaleri").css('display','block');
    });

    $( "#noGaleri" ).on( "click", function() {
        $("#addgaleri").css('display','none');
    });

    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

    $('#galeri').fileuploader({
        addMore: true
    });

    $(document).ready(function () {
        jQuery.validator.addMethod("usrregex", function(value, element){
            if (/[^a-zA-Z0-9]/.test(value)) {
                return false;
            } else {
                return true;
            };
        }, "Oops, Gunakan karakter yang benar.. "); 

        $("#formAdd").validate({
            rules: {
                url: {
                    required: true,
                    minlength: 3,
                    maxlength: 15,
                    usrregex: true,
                    remote: {
                        url: "<?php echo base_url('dasbor/cekslug') ?>",
                        type: "post",
                        data: {
                            slug: function() {
                                return $( "#url" ).val();
                            }
                        }
                    }
                }
            },
            messages: {
                url: {
                    remote: "Oops, URL sudah terpakai",
                    regex: "Oops, harap menggunakan karakter yang benar"
                }
            }
        });
    });
</script>

</body>
</html>
