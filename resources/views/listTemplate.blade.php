<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ADMIN MEMONIKA</title>
    @extends('parts.style')
    <link rel="stylesheet" href="{{ asset('assets/admin/css/jquery.fileuploader.css') }}">
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
@extends('parts.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="margin-top: 0 !important">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2 mt-5">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">List Template</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="">Home</a></li>
                    <li class="breadcrumb-item active">Tambah Template</li>
                    </ol>
                </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    @foreach($template as $i => $t)
                    <div class="col-md-3 col-6">
                        <div class="card">
                            <img src="<?= $t->ui_desktop ?>" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title"><?= $t->filename ?></h5>
                                <br><hr>
                                <a href="{{ URL('example/'.$t->filename) }}" target="blank" class="btn btn-primary btn-sm">Preview</a>
                                <a href="('invitation/add/?t='.$t->filename.'&r=homepage')" class="btn btn-primary btn-sm">Buat</a>
                            </div>
                        </div>
                    </div>
                    @endforeach;
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->


    <!-- Main Footer -->
    <footer class="main-footer">
        <strong>Copyright &copy; <?= date('Y') ?> akadin.ID</strong>
        All rights reserved.
        <div class="float-right d-none d-sm-inline-block">
        Powered by <b>Gidicode Project</b>
        </div>
    </footer>
</div>
<!-- ./wrapper -->

@extends('parts.script')

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="{{ asset('assets/admin/plugins/jquery-mousewheel/jquery.mousewheel.js') }}" defer></script>
<script src="{{ asset('assets/admin/plugins/raphael/raphael.min.js') }}" defer></script>
<script src="{{ asset('assets/admin/plugins/jquery-mapael/jquery.mapael.min.js') }}" defer></script>
<script src="{{ asset('assets/admin/plugins/jquery-mapael/maps/usa_states.min.js') }}" defer></script>
<!-- ChartJS -->
<script src="{{ asset('assets/admin/plugins/chart.js/Chart.min.js') }}" defer></script>

<!-- PAGE SCRIPTS -->
<script src="{{ asset('assets/admin/js/pages/dashboard2.js') }}" defer></script>
<script src="{{ asset('assets/admin/js/jquery.fileuploader.js') }}" defer></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>


</body>
</html>
