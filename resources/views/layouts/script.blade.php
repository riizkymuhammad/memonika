<!-- loader -->
<div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>

<script src="{{ asset('front/js/jquery.min.js') }}" defer></script>
<script src="{{ asset('assets/front/js/jquery-migrate-3.0.1.min.js') }}" defer></script>
<script src="{{ asset('assets/front/js/popper.min.js') }}" defer></script>
<script src="{{ asset('assets/front/js/bootstrap.min.js') }}" defer></script>
<script src="{{ asset('assets/front/js/jquery.easing.1.3.min.js') }}" defer></script>
<script src="{{ asset('assets/front/js/jquery.waypoints.min.js') }}" defer></script>
<script src="{{ asset('assets/front/js/jquery.stellar.min.js') }}" defer></script>
<script src="{{ asset('assets/front/js/owl.carousel.min.js') }}" defer></script>
<script src="{{ asset('assets/front/js/jquery.magnific-popup.min.js') }}" defer></script>
<script src="{{ asset('assets/front/js/aos.js') }}" defer></script>
<script src="{{ asset('assets/front/js/jquery.animateNumber.min.js') }}" defer></script>
<script src="{{ asset('assets/front/js/scrollax.min.js') }}" defer></script>
<script src="{{ asset('assets/front/js/google-map.js') }}" defer></script>
<script src="{{ asset('assets/front/js/main.js') }}" defer></script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
