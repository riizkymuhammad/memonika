<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Memonika - Undangan Nikah Digital</title>
        @extends('layouts.head')
    </head>
    <body>
        
    @extends('layouts.header')

        <div class="hero-wrap hero-wrap-2" style="background-image: url('images/bg_2.jpg');" data-stellar-background-ratio="0.5">
            <div class="overlay"></div>
            <div class="container">
                <div class="row slider-text align-items-center">
                    <div class="ftco-animate mt-5">
                        <h3 class="text-white">Buat Undangan</h3>
                        <p class="breadcrumbs mb-0">
                            <span class="mr-3"><a href="{{ route('create') }}">Pilih tema <i class="ion-ios-arrow-forward"></i></a></span> 
                            <span class="mr-3"><a href="{{ route('createAddon') }}">Pilih paket tambahan <i class="ion-ios-arrow-forward"></i></a></span>
                            <span class="mr-3">Edit Undangan <i class="ion-ios-arrow-forward"></i></span>
                            <span class="mr-3">Pembayaran <i class="ion-ios-arrow-forward"></i></span>
                            <span>Undangan Terbit</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        @section('content')
        <section class="ftco-section bg-light pb-5 pt-5">
            <div class="container ftco-animate">
                <form action="#" method="get">
                <div class="row">
                    <div class="col-md-4 col-12 mb-5">
                        <div class="d-flex">
                            <input type="hidden" name="template_val" id="template_val" value="150000" />
                            <img src="<?= ('assets/uploads/A11.jpg') ?>" class="mr-3" alt="" style="width: 120px">
                            <img src="" class="mr-3" alt="" style="width: 120px">
                            <div class="text">
                            <h6>Template A1</h6>
                            <p>Rp. 150,000</p>
                            <a href="#">Ganti template</a>
                            </div>
                        </div>
                        <hr>
                        <div class="d-none d-sm-block">
                            <h5>Total Harga</h5>
                            <table class="table table-sm text-left">
                                <tbody>
                                    <tr>
                                        <td>Template</td>
                                        <td id="template"></td>
                                    </tr>
                                    <tr>
                                        <input type="hidden" id="ytb1" value="0">
                                        <input type="hidden" id="comm1" value="0">
                                        <td>Addon</td>
                                        <td id="addon"></td>
                                    </tr>
                                    <tr>
                                        <td>Total</td>
                                        <td id="total"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-8 col-12">
                        <h5>Addon</h5>
                        <input type="hidden" name="template" value="a1">
                        <input type="hidden" value="40000" id="ytb_val">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="ytb" name="ytb" value="1">
                            <label class="custom-control-label" for="ytb" style="line-height:1.2">
                                <strong>Cerita Cinta</strong> <br>
                                <small>Rp. 40,000</small> <br>
                                Tambahkan fitur ini jika ingin menambahkan cerita kisah cintamu dengan pasanganmu sejak pertama kali kalian bertemu
                            </label>
                        </div>
                        <hr>
                        <input type="hidden" id="ytb_val" value="40000">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="ytb" name="ytb" value="1">
                            <label class="custom-control-label" for="comm" style="line-height:1.2">
                                <strong>Video Galeri</strong> <br>
                                <small>Rp. 40,000</small> <br>
                                Tambahkan fitur ini jika ingin menambahkan undanganmu dengan membagikan momen behagia yang sudah diabadikan dalam bentuk video
                            </label>
                        </div>
                        <hr>
                        <input type="hidden" id="ytb_val" value="30000">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="smb" name="smb" value="1">
                            <label class="custom-control-label" for="smb" style="line-height:1.2">
                                <strong>RSVP (Konfirmasi Kehadirian)</strong> <br>
                                <small>Rp. 30.000</small> <br>
                                Tambahkan fitur ini jika ingin menambahkan konfirmasi kehadiran para undangan nantinya dipernikahan kamu
                            </label>
                        </div>
                        <hr>
                        <input type="hidden" id="comm_val" value="30000">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="comm" name="comm" value="1">
                            <label class="custom-control-label" for="comm" style="line-height:1.2">
                                <strong>Friend Wishes</strong> <br>
                                <small>Rp. 30,000</small> <br>
                                Tambahkan fitur ini jika ingin para undangan dapat mengucapkan doa dan ucapan kepadamu dan terlihat diundanganmu
                            </label>
                        </div>
                        <hr>
                        <input type="hidden" id="comm_val" value="30000">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="smb" name="smb" value="1">
                            <label class="custom-control-label" for="smb" style="line-height:1.2">
                                <strong>Music by Request</strong> <br>
                                <small>Rp. 30.000</small> <br>
                                Tambahkan fitur ini jika ingin backsound musik di undanganmu sesuai dengan yang kamu inginkan
                            </label>
                        </div>
                        <hr>
                        <input type="hidden" id="comm_val" value="150000">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="comm" name="comm" value="1">
                            <label class="custom-control-label" for="comm" style="line-height:1.2">
                                <strong>Custom Domain</strong> <br>
                                <small>Rp. 150,000</small> <br>
                                Kamu dapat menggunakan nama alamat domain undangan digitalmu sesuai keinginanmu
                            </label>
                        </div>
                        <div class="mt-5"></div>
                        <button type="submit" class="btn btn-primary float-right">Lanjutkan</button>
                    </div>
                </div>
                </form>
            </div>
        </section>

        @extends('layouts.footer')
    @extends('layouts.script')
        
    @push('script')
        <script>
            $( document ).ready(function() {
                var ytb = 0;
                var comm = 0;
                var template = parseInt($("#template_val").val());
                $("#template").html("Rp. "+template);

                $("#ytb").change(function() {
                    
                    if ($("#ytb").is(':checked')) {
                        $("#ytb1").val(parseInt($("#ytb_val").val()));
                        
                    } else {
                        $("#ytb1").val(0);
                    }
                    var total_addon = parseInt($("#ytb1").val()) + parseInt($("#comm1").val());
                    var total = total_addon + template;
                    $("#addon").text("Rp. "+total_addon);
                    $("#total").text("Rp. "+total);
                });

                $("#comm").change(function() {
                    if ($("#comm").is(':checked')) {
                        $("#comm1").val(parseInt($("#comm_val").val()));
                    } else {
                        $("#comm1").val(0);
                    }

                    var total_addon = parseInt($("#ytb1").val()) + parseInt($("#comm1").val());
                    var total = total_addon + template;
                    $("#addon").text("Rp. "+total_addon);
                    $("#total").text("Rp. "+total);
                });
                var total = template;
                $("#addon").html("Rp. 0");
                $("#total").html("Rp. "+total);
            });
        </script>
          @endpush


    </body>
</html>