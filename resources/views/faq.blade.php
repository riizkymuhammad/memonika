<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Memonika - Undangan Nikah Digital</title>
        @extends('layouts.head')
    </head>
    <body>
    @extends('layouts.header')

    
        <section class="ftco-section services-section bg-light ftco-no-pb">
            <div class="container">
                <div class="row justify-content-center mb-5 pb-3">
                <div class="col-md-8 text-center heading-section ftco-animate">
                <h1 class="mb-3 h3">FAQ</h1>
                <h1 class="mb-3 h3">(Frequently Asked Questions)</h1>
                Kami senang dapat memandu anda untuk membagikan momen bahagia anda dengan cara 
yang lebih mudah, silahkan lihat jawaban atas pertanyaan yang sering ditanyakan tentang 
layanan kami, jika mengalami kendala kami selalu ada untuk anda, kontak kami di 
memonikah.id@gmail.com / (Whatsapp) 082133116233
                </div>
                </div>

                <div class="row justify-content-center mb-5 pb-3">
                <div class="col-md-12 text-left heading-section ftco-animate">
                <h1 class="mb-3 h3">Apakah Ada Batasan Revisi ?</h1>
                Tidak ada batasan revisi untuk setiap orderan undangan yang dilakukan, dengan catatan revisi hanya 
meliputi perubahan kata-kata, nama, lokasi, foto, video, tanggal, hari, jam, email RSVP. Biaya akan 
dikenakan jika melakukan revisi full desain, seperti mengganti desain undangan awal (Desain a1 ke a2) 
akan dikenakan biaya tambahan sesuai harga undangan yang dipilih.
                </div>
                </div>

                <div class="row justify-content-center mb-5 pb-3">
                <div class="col-md-12 text-left heading-section ftco-animate">
                <h1 class="mb-3 h3">Berapa Lama Undangan Akan Aktif / Online ?</h1>
                Tidak ada batasan waktu tanggal berakhir undangan selagi memonika masih add.
                </div>
                </div>

                <div class="row justify-content-center mb-5 pb-3">
                <div class="col-md-12 text-left heading-section ftco-animate">
                <h1 class="mb-3 h3">Apakah Data Undangan Yang Salah Dapat Diedit Kembali ?</h1>
                Tentu saja kesalahan data atau perubahan susunan acara dapat diedit kembali, seperti kesalahan 
penulisan gelar, nama, lokasi, tanggal dan lainnya, tim kami akan berusaha seresponsif mungkin 
untuk memperbaiki data undangan kalian
                </div>
                </div>

                <div class="row justify-content-center mb-5 pb-3">
                <div class="col-md-12 text-left heading-section ftco-animate">
                <h1 class="mb-3 h3">Apakah Data Undangan Yang Salah Dapat Diedit Kembali ?</h1>
                Tentu saja kalian dapat menambah fitur yang belum ada di undangan yang kalian pilih, misalnya 
kalian ingin menggunakan undangan “a1” tetapi kalian ingin menambah fitur video, rsvp dan 
friend wishes. Kalian hanya perlu mencentang fitur yang ingin ditambahkan saat mengisi form order
                </div>
                </div>

        </section>

        <!-- 
        <section class="ftco-section ftco-counter bg-light img" id="section-counter">
            <div class="container">
                <div class="row justify-content-center mb-5">
            <div class="col-md-10 text-center heading-section ftco-animate">
                <h2 class="mb-4"></h2>
            </div>
            </div>
                <div class="row justify-content-center">
            <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18 text-center">
                <div class="text">
                    <strong class="number" data-number="12000">0</strong>
                    <span>CMS Installation</span>
                </div>
                </div>
            </div>
            <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18 text-center">
                <div class="text">
                    <strong class="number" data-number="100">0</strong>
                    <span>Awards Won</span>
                </div>
                </div>
            </div>
            <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18 text-center">
                <div class="text">
                    <strong class="number" data-number="10000">0</strong>
                    <span>Registered Domains</span>
                </div>
                </div>
            </div>
            <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18 text-center">
                <div class="text">
                    <strong class="number" data-number="9000">0</strong>
                    <span>Satisfied Customers</span>
                </div>
                </div>
            </div>
            </div>
            </div>
        </section>

        <section class="ftco-section ftco-no-pt ftco-no-pb">
            <div class="container">
                <div class="row d-flex">
                    <div class="col-lg-6 order-lg-last d-flex">
                        <div class="bg-primary py-md-5 d-flex align-self-stretch">
                            <div class="main">
                                <img src="images/undraw_data_report_bi6l.svg" class="img-fluid svg" alt="">
                                <div class="heading-section heading-section-white ftco-animate mt-5 px-3 px-md-5">
                            <h2 class="mb-4">Our Main Services</h2>
                            <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
                        </div>
                    </div>
                </div>
                    </div>
                    <div class="col-lg-6 py-5">
                        <div class="row pt-md-5">
                            <div class="col-md-6 ftco-animate">
                                <div class="media block-6 services text-center">
                            <div class="icon d-flex align-items-center justify-content-center">
                                <span class="flaticon-cloud-computing"></span>
                            </div>
                        <div class="mt-3 media-body media-body-2">
                            <h3 class="heading">Cloud VPS</h3>
                            <p>Even the all-powerful Pointing has no control about the blind texts</p>
                        </div>
                        </div>
                            </div>
                            <div class="col-md-6 ftco-animate">
                                <div class="media block-6 services text-center">
                            <div class="icon d-flex align-items-center justify-content-center">
                                <span class="flaticon-cloud"></span>
                            </div>
                        <div class="mt-3 media-body media-body-2">
                            <h3 class="heading">Share</h3>
                            <p>Even the all-powerful Pointing has no control about the blind texts</p>
                        </div>
                        </div>
                            </div>
                            <div class="col-md-6 ftco-animate">
                                <div class="media block-6 services text-center">
                            <div class="icon d-flex align-items-center justify-content-center">
                                <span class="flaticon-server"></span>
                            </div>
                        <div class="mt-3 media-body media-body-2">
                            <h3 class="heading">VPS</h3>
                            <p>Even the all-powerful Pointing has no control about the blind texts</p>
                        </div>
                        </div>
                            </div>
                            <div class="col-md-6 ftco-animate">
                                <div class="media block-6 services text-center">
                            <div class="icon d-flex align-items-center justify-content-center">
                                <span class="flaticon-database"></span>
                            </div>
                        <div class="mt-3 media-body media-body-2">
                            <h3 class="heading">Dedicated</h3>
                            <p>Even the all-powerful Pointing has no control about the blind texts</p>
                        </div>
                        </div>
                            </div>
                        </div>
            </div>
                </div>
            </div>
        </section>

        <section class="ftco-section bg-primary">
            <div class="container">
                <div class="row justify-content-center mb-5 pb-3">
            <div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
                <h2 class="mb-4">Harga Undangan</h2>
                <p>Dapatkan undangan online terbaik dengan harga mulai dari Rp. 0,-</p>
            </div>
            </div>
                <div class="row no-gutters d-flex">
                <div class="col-lg-3 col-md-6 ftco-animate d-flex">
                <div class="block-7 bg-light d-flex align-self-stretch">
                    <div class="text-center">
                        <h2 class="heading">BASIC</h2>
                        <span class="price"><sup>Rp.</sup> <span class="number">0</span>
                        <span class="excerpt d-block">100% gratis</span>
                        
                        <ul class="pricing-text mb-4">
                        <li>Nama dan foto mempelai</li>
                        <li>Lokasi akad dan resepsi dengan Google Maps</li>
                        <li>Desain premium</li>
                        <li>Galeri foto</li>
                        </ul>
                        <a href="#" class="btn btn-secondary d-block px-3 py-3 mb-4">Pilih</a>
                    </div>
                </div>
                </div>
                <div class="col-lg-3 col-md-6 ftco-animate d-flex">
                <div class="block-7 d-flex align-self-stretch">
                    <div class="text-center">
                        <h2 class="heading">PREMIUM</h2>
                        <span class="price"><sup>Rp.</sup> <span class="number">150,000</span></span>
                        <h3 class="heading-2 mb-3">Seluruh Fitur Basic</h3>
                        
                        <ul class="pricing-text mb-4">
                        <li>Galeri Foto dan Video Youtube</li>
                        <li>Lagu pengiring</li>
                        <li>Komentar dan konfirmasi kehadiran</li>
                        </ul>
                        <a href="#" class="btn btn-secondary d-block px-3 py-3 mb-4">Choose Plan</a>
                    </div>
                </div>
                </div>
                <div class="col-lg-3 col-md-6 ftco-animate d-flex">
                <div class="block-7 active d-flex align-self-stretch">
                    <div class="text-center">
                        <h2 class="heading">ROYAL WEDDING</h2>
                        <span class="price"><span class="number">Hubungi Kami</span></span>
                        <span class="excerpt d-block">All features are included</span>
                        <h3 class="heading-2 mb-3">Enjoy All The Features</h3>
                        
                        <ul class="pricing-text mb-4">
                        <li><strong>250 GB</strong> Bandwidth</li>
                        <li><strong>200 GB</strong> Storage</li>
                        <li><strong>$5.00 / GB</strong> Overages</li>
                        <li>All features</li>
                        </ul>
                        <a href="#" class="btn btn-primary d-block px-3 py-3 mb-4">Choose Plan</a>
                    </div>
                </div>
                </div>
                <div class="col-lg-3 col-md-6 ftco-animate d-flex">
                <div class="block-7 d-flex align-self-stretch">
                    <div class="text-center">
                        <h2 class="heading">Pro</h2>
                        <span class="price"><sup>$</sup> <span class="number">99<small class="per">/mo</small></span></span>
                        <span class="excerpt d-block">All features are included</span>
                        <h3 class="heading-2 mb-3">Enjoy All The Features</h3>
                        
                        <ul class="pricing-text mb-4">
                        <li><strong>450 GB</strong> Bandwidth</li>
                        <li><strong>400 GB</strong> Storage</li>
                        <li><strong>$20.00 / GB</strong> Overages</li>
                        <li>All features</li>
                        </ul>
                        <a href="#" class="btn btn-secondary d-block px-3 py-3 mb-4">Choose Plan</a>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </section> -->

   

        <!-- 
        <section class="ftco-section bg-light">
            <div class="container">
                <div class="row justify-content-center mb-5 pb-3">
                <div class="col-md-7 text-center heading-section ftco-animate">
                    <h2>Recent Blog</h2>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in</p>
                </div>
                </div>
                <div class="row">
                <div class="col-md-4 ftco-animate">
                    <div class="blog-entry">
                    <a href="blog-single.html" class="block-20" style="background-image: url('images/image_1.jpg');">
                    </a>
                    <div class="text text-center py-3">
                        <div class="meta mb-2">
                        <div><a href="#">Aug 5, 2019</a></div>
                        <div><a href="#">Admin</a></div>
                        <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                        </div>
                        <div class="desc">
                            <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-md-4 ftco-animate">
                    <div class="blog-entry" data-aos-delay="100">
                    <a href="blog-single.html" class="block-20" style="background-image: url('images/image_2.jpg');">
                    </a>
                    <div class="text text-center py-3">
                        <div class="meta mb-2">
                        <div><a href="#">Aug 5, 2019</a></div>
                        <div><a href="#">Admin</a></div>
                        <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                        </div>
                        <div class="desc">
                            <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-md-4 ftco-animate">
                    <div class="blog-entry" data-aos-delay="200">
                    <a href="blog-single.html" class="block-20" style="background-image: url('images/image_3.jpg');">
                    </a>
                    <div class="text text-center py-3">
                        <div class="meta mb-2">
                        <div><a href="#">Aug 5, 2019</a></div>
                        <div><a href="#">Admin</a></div>
                        <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                        </div>
                        <div class="desc">
                            <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </div>
        </section>
    -->
    @extends('layouts.footer')
    @extends('layouts.script')
    </body>
</html>
