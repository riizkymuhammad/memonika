<?php if($lagu != ""){ ?>
<div class="music">
    <audio src="<?= $lagu ?>" id="audio"></audio>
    <a class="fas fa-music" onclick="play()" id="btnplay" style="
        position: fixed !important; 
        bottom: 22%;
        right: 20px;
        background-color: #f0f0f0;
        padding: 15px;
        border-radius: 100%;
        font-size: 14pt;
        cursor: pointer;
        z-index: 999;
        " 
    ></a>
    <a class="fas fa-volume-mute" onclick="stop()" id="btnstop" style="
        display:none;
        position: fixed !important; 
        bottom: 22%;
        right: 20px;
        background-color: #f0f0f0;
        padding: 15px;
        border-radius: 100%;
        font-size: 14pt;
        cursor: pointer;
        z-index: 999;
        " 
    ></a>
    <script>
        function play() {
            var audio = document.getElementById('audio');
            var btnPlay = document.getElementById('btnplay');
            var btnStop = document.getElementById('btnstop');

            audio.play();
            btnPlay.style.display = "none";
            btnStop.style.display = "block";
        }

        function stop() {
            var audio = document.getElementById('audio');
            var btnPlay = document.getElementById('btnplay');
            var btnStop = document.getElementById('btnstop');

            audio.pause();
            btnStop.style.display = "none";
            btnPlay.style.display = "block";
        }
    </script>
</div>
<?php } ?>

<div class="container " style="margin-bottom: 60px; margin-top: 50px" id="comments">
    <div class="card">
        <div class="card-body" >
            <form action="" method="post"> 
                <input type="hidden" name="invitation_id" value="<?= $data['wedding']->invitation_id ?>" />
                <h4 class="text-center">Konfirmasi Kehadiran</h4>
                <div class="row">
                    <div class="col-md-6 form-group">
                        <label for="">Nama Lengkap</label>
                        <input type="text" class="form-control" name="fullname">
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="">Saya akan</label>
                        <select name="is_attend" id="" class="form-control">
                            <option value="1">Hadir</option>
                            <option value="2">Mungkin</option>
                            <option value="3">Tidak Hadir</option>
                        </select>
                    </div>
                    <div class="col-md-12 form-group">
                        <label for="">Komentar</label>
                        <textarea name="comment" id="" class="form-control"> </textarea>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary float-right">Kirim</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <hr>

</div>