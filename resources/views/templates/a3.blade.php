<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Wedding <?= $data['wedding']->nickname_man ?> dan <?= $data['wedding']->nickname_woman ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="" />
	<meta name="keywords" content="wedding invitation" />
	<meta name="author" content="Gidicode Project" />

  <!-- 
	//////////////////////////////////////////////////////

	FREE HTML5 TEMPLATE 
	DESIGNED & DEVELOPED by FREEHTML5.CO
		
	Website: 		http://freehtml5.co/
	Email: 			info@freehtml5.co
	Twitter: 		http://twitter.com/fh5co
	Facebook: 		https://www.facebook.com/fh5co

	//////////////////////////////////////////////////////
	 -->
	 @extends('a.style')
    <link href="https://fonts.googleapis.com/css?family=Pacifico|Sofia&display=swap" rel="stylesheet">
    <style>
        .fh5co-cover .display-t, .fh5co-cover .display-tc {
            height: 100vh !important;
        }

        @media screen and (max-width: 480px) {
            .fh5co-cover .display-t h1 {
                font-size: 32pt !important;
            }

            #fh5co-header h3 {
                margin-top: 0 !important;
            }

            .couple-half.ch2{
                width: 50%;
                float: left;
            }

            .heart.hrt2 {
                display: block !important;
            }
        }
    </style>
	</head>
	<body>
		
	<div class="fh5co-loader"></div>
	
	<div id="page">

		<header id="fh5co-header" class="fh5co-cover" role="banner" style="background-image:url('{{ asset('assets/template/b/img/bg5.jpg') }}'); " data-stellar-background-ratio="0.5">
			
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center">
						<div class="display-t">
							<div class="display-tc animate-box" data-animate-effect="fadeIn">
                                <h3 style="font-family: 'Sofia', cursive; color: #F14E95">Wedding invitation</h3>
								<h1 style="font-family: 'Sofia', cursive; color: #F14E95; font-size: 60pt"><?= $data['wedding']->nickname_man ?> &amp; <?= $data['wedding']->nickname_woman ?></h1>
								<div class="couple-wrap animate-box" style="margin-top: 40px !important">
                                    <div class="couple-half ch2">
                                        <div class="groom">
                                            <img src="<?= $data['wedding']->foto_pria ?>" alt="groom" class="img-responsive">
                                        </div>
                                    </div>
                                    <p class="heart hrt2 text-center"><i class="icon-heart2"></i></p>
                                    <div class="couple-half ch2">
                                        <div class="bride">
                                            <img src="<?= $data['wedding']->foto_wanita ?>" alt="groom" class="img-responsive">
                                        </div>
                                    </div>
                                </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>

		<div id="fh5co-couple">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
                        <?php if($data['wedding']->withBasmallah == 1){ ?>
                            <h4 style="font-family: 'Tajawal', sans-serif;">بِسْمِ اللَّهِ الرَّحْمَنِ الرَّحِيم</h4>
						    <h2>Assalamu'alaikum</h2>
                        <?php } else {  ?>
                            <h2>You are invited</h2>
                        <?php } ?>
						<p>We invited you to celebrate our wedding</p>
						<h4><?= date('d F Y', strtotime($data['wedding']->tanggal_resepsi)).', '.$data['wedding']->kota ?></h4>
					</div>
				</div>
				<div class="couple-wrap animate-box">
					<div class="couple-half">
						<div class="groom">
							<img src="<?= $data['wedding']->foto_pria ?>" alt="groom" class="img-responsive">
						</div>
						<div class="desc-groom">
							<h3 style="font-family: 'Sofia', cursive;"><?= $data['wedding']->fullname_man ?></h3>
							<p><?= $data['wedding']->desc_man ?></p>
						</div>
					</div>
					<p class="heart text-center"><i class="icon-heart2"></i></p>
					<div class="couple-half">
						<div class="bride">
							<img src="<?= $data['wedding']->foto_wanita ?>" alt="groom" class="img-responsive">
						</div>
						<div class="desc-bride">
							<h3 style="font-family: 'Sofia', cursive;"><?= $data['wedding']->fullname_woman ?></h3>
							<p><?= $data['wedding']->desc_woman ?></p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="fh5co-event" class="fh5co-bg" style="background-image:url(<?= $data['wedding']->foto_sampul ?>); background-position: center; background-size: cover">
			<div class="overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
						<span>Our Special Events</span>
						<h2>Wedding Events</h2>
					</div>
				</div>
				<div class="row">
					<div class="display-t">
						<div class="display-tc">
							<div class="<?php if($data['wedding']->withAkad == 0) { echo "col-md-6 col-md-offset-3"; } else { echo "col-md-10 col-md-offset-1"; } ?>">
                                <?php if($data['wedding']->withAkad == 1) { ?>
								<div class="col-md-6 col-sm-6 text-center">
									<div class="event-wrap animate-box">
										<h3>Akad Nikah</h3>
										<div class="event-col">
											<i class="icon-clock"></i>
											<span><?= $data['wedding']->jam_akad_nikah ?></span>
										</div>
										<div class="event-col">
											<i class="icon-calendar"></i>
											<span><?= date('l', strtotime($data['wedding']->tanggal_akad_nikah)); ?></span>
											<span><?= date('d M Y', strtotime($data['wedding']->tanggal_akad_nikah)); ?></span>
										</div>
										<h4 style="color: #fff !important"><?= $data['wedding']->lokasi_akad_nikah ?></h4>
										<p><?= $data['wedding']->alamat_akad_nikah ?></p>
										<a href="<?= $data['wedding']->maps_akad_nikah ?>" target="_blank" class="btn btn-primary">Lihat Peta</a>
									</div>
                                </div>
                                <?php } ?>
								<div class="<?php if($data['wedding']->withAkad == 0) { echo "col-md-12 col-sm-12 mx-auto"; } else { echo "col-md-6 col-sm-6"; } ?> text-center">
									<div class="event-wrap animate-box">
										<h3>Resepsi</h3>
										<div class="event-col">
											<i class="icon-clock"></i>
											<span><?= $data['wedding']->jam_resepsi ?></span>
										</div>
										<div class="event-col">
                                            <i class="icon-calendar"></i>
                                            <span><?= date('l', strtotime($data['wedding']->tanggal_resepsi)); ?></span>
											<span><?= date('d M Y', strtotime($data['wedding']->tanggal_resepsi)); ?></span>
                                        </div>
                                        <h4 style="color: #fff !important"><?= $data['wedding']->lokasi_resepsi ?></h4>
										<p><?= $data['wedding']->alamat_resepsi ?></p>
										<a href="<?= $data['wedding']->maps_resepsi ?>" target="_blank" class="btn btn-primary">Lihat Peta</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
        
		<div id="fh5co-gallery" class="fh5co-section-gray">
            <?php if($data['wedding']->withGaleri == 1){ ?>
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
						<span>Our Memories</span>
                        <h2>Wedding Gallery</h2>
                    </div>
				</div>
				<div class="top">
					<ul>
					@foreach($get_img as $i => $img)
						<li><a href="#img_<?= $i ?>"><img src="<?= $img->filename ?>"></a></li>
                        @endforeach; ?>
                    </ul>
                    <?php foreach($get_img as $i => $img): ?> 
					<a href="#_<?= $i ?>" class="lightbox trans" id="img_<?= $i ?>"><img src="<?= $img->filename ?>"></a>
                    <?php endforeach; ?>
				</div>
            </div>
            <?php } ?>
        </div>

		@extends('templates.parts.a.footer')
		
	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
	
	@extends('templates.parts.a.script')

	<script>
    var d = new Date('<?= date('m/d/y', strtotime($data['wedding']->tanggal_resepsi)) ?>');

    // default example
    simplyCountdown('.simply-countdown-one', {
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate()
	});
	
</script>

	</body>
</html>

