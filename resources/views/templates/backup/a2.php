<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Wedding <?= $wedding['nickname_man'] ?> dan <?= $wedding['nickname_woman'] ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="" />
	<meta name="keywords" content="wedding invitation" />
	<meta name="author" content="Gidicode Project" />

  <!-- 
	//////////////////////////////////////////////////////

	FREE HTML5 TEMPLATE 
	DESIGNED & DEVELOPED by FREEHTML5.CO
		
	Website: 		http://freehtml5.co/
	Email: 			info@freehtml5.co
	Twitter: 		http://twitter.com/fh5co
	Facebook: 		https://www.facebook.com/fh5co

	//////////////////////////////////////////////////////
	 -->
    <?php $this->load->view('templates/parts/a/style') ?>
	</head>
	<body>
		
	<div class="fh5co-loader"></div>
	
	<div id="page">

    	<header id="fh5co-header" class="fh5co-cover" role="banner" style="background-image:url('<?= base_url('assets/template/a/images/bg2.jpg') ?>'); " data-stellar-background-ratio="0.5">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center">
						<div class="display-t t1">
							<div class="display-tc animate-box" data-animate-effect="fadeIn" style="background-image:url('<?= base_url('assets/template/a/images/or1.png') ?>'); background-size: 100%; background-repeat: no-repeat;">
								<h1 style="color:rgb(175, 150, 9); line-height: 1;"><?= $wedding['nickname_man'] ?> &amp; <?= $wedding['nickname_woman'] ?></h1>
								<h2 style="color:rgb(175, 150, 9)">We Are Getting Married</h2>
                            </div>
                        </div>
                        <h3 style="color:rgb(175, 150, 9); margin-top: -250px"><?= $wedding['kota'].', '.date('d F Y', strtotime($wedding['tanggal_resepsi'])) ?></h3>
					</div>
				</div>
			</div>
		</header>

		<div id="fh5co-couple">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
                        <?php if($wedding['withBasmallah'] == 1){ ?>
                            <h4 style="font-family: 'Tajawal', sans-serif;">بِسْمِ اللَّهِ الرَّحْمَنِ الرَّحِيم</h4>
						    <h2>Assalamu'alaikum</h2>
                        <?php } else {  ?>
                            <h2>You are invited</h2>
                        <?php } ?>
						<p>We invited you to celebrate our wedding</p>
						<h4><?= date('d F Y', strtotime($wedding['tanggal_resepsi'])).', '.$wedding['kota'] ?></h4>
					</div>
				</div>
				<div class="couple-wrap animate-box">
					<div class="couple-half">
						<div class="groom">
							<img src="<?= $wedding['foto_pria'] ?>" alt="groom" class="img-responsive">
						</div>
						<div class="desc-groom">
							<h3><?= $wedding['fullname_man'] ?></h3>
							<p><?= $wedding['desc_man'] ?></p>
						</div>
					</div>
					<p class="heart text-center"><i class="icon-heart2"></i></p>
					<div class="couple-half">
						<div class="bride">
							<img src="<?= $wedding['foto_wanita'] ?>" alt="groom" class="img-responsive">
						</div>
						<div class="desc-bride">
							<h3><?= $wedding['fullname_woman'] ?></h3>
							<p><?= $wedding['desc_woman'] ?></p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="fh5co-event" class="fh5co-bg" style="background-image:url(images/img_bg_3.jpg);">
			<div class="overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
						<span>Our Special Events</span>
						<h2>Wedding Events</h2>
					</div>
				</div>
				<div class="row">
					<div class="display-t">
						<div class="display-tc">
							<div class="<?php if($wedding['withAkad'] == 0) { echo "col-md-6 col-md-offset-3"; } else { echo "col-md-10 col-md-offset-1"; } ?>">
                                <?php if($wedding['withAkad'] == 1) { ?>
								<div class="col-md-6 col-sm-6 text-center">
									<div class="event-wrap animate-box">
										<h3>Akad Nikah</h3>
										<div class="event-col">
											<i class="icon-clock"></i>
											<span><?= $wedding['jam_akad_nikah'] ?></span>
										</div>
										<div class="event-col">
											<i class="icon-calendar"></i>
											<span><?= date('l', strtotime($wedding['tanggal_akad_nikah'])); ?></span>
											<span><?= date('d M Y', strtotime($wedding['tanggal_akad_nikah'])); ?></span>
										</div>
										<h4 style="color: #fff !important"><?= $wedding['lokasi_akad_nikah'] ?></h4>
										<p><?= $wedding['alamat_akad_nikah'] ?></p>
										<a href="<?= $wedding['maps_akad_nikah'] ?>" target="_blank" class="btn btn-primary">Lihat Peta</a>
									</div>
                                </div>
                                <?php } ?>
								<div class="<?php if($wedding['withAkad'] == 0) { echo "col-md-12 col-sm-12 mx-auto"; } else { echo "col-md-6 col-sm-6"; } ?> text-center">
									<div class="event-wrap animate-box">
										<h3>Resepsi</h3>
										<div class="event-col">
											<i class="icon-clock"></i>
											<span><?= $wedding['jam_resepsi'] ?></span>
										</div>
										<div class="event-col">
                                            <i class="icon-calendar"></i>
                                            <span><?= date('l', strtotime($wedding['tanggal_resepsi'])); ?></span>
											<span><?= date('d M Y', strtotime($wedding['tanggal_resepsi'])); ?></span>
                                        </div>
                                        <h4 style="color: #fff !important"><?= $wedding['lokasi_resepsi'] ?></h4>
										<p><?= $wedding['alamat_resepsi'] ?></p>
										<a href="<?= $wedding['maps_resepsi'] ?>" target="_blank" class="btn btn-primary">Lihat Peta</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
        
		<div id="fh5co-gallery" class="fh5co-section-gray">
            <?php if($wedding['withGaleri'] == 1){ ?>
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
						<span>Our Memories</span>
                        <h2>Wedding Gallery</h2>
                    </div>
				</div>
				<div class="top">
					<ul>
                        <?php 
                        $get_img = $this->db->get_where('invitation_gallery',array('invitation_id' => $wedding['id']));
                        $count = $get_img->num_rows();
                        foreach($get_img->result() as $i => $img): ?>
						<li><a href="#img_<?= $i ?>"><img src="<?= $img->filename ?>"></a></li>
                        <?php endforeach; ?>
                    </ul>
                    <?php foreach($get_img->result() as $i => $img): ?> 
					<a href="#_<?= $i ?>" class="lightbox trans" id="img_<?= $i ?>"><img src="<?= $img->filename ?>"></a>
                    <?php endforeach; ?>
				</div>
            </div>
            <?php } ?>
        </div>

        <?php $this->load->view('templates/parts/a/footer') ?>
		
	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
	
	<?php $this->load->view('templates/parts/a/script') ?>

	<script>
    var d = new Date('<?= date('m/d/y', strtotime($wedding['tanggal_resepsi'])) ?>');

    // default example
    simplyCountdown('.simply-countdown-one', {
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate()
    });

    //jQuery example
    $('#simply-countdown-losange').simplyCountdown({
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate(),
        enableUtc: false
    });
</script>

	</body>
</html>

