<!DOCTYPE HTML>
<html lang="en">
<head>
	<title>Wedding <?= $wedding['nickname_man'] ?> dan <?= $wedding['nickname_woman'] ?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8">
	
	
	<!-- Font -->
	
	<link href="https://fonts.googleapis.com/css?family=Playball%7CBitter" rel="stylesheet">
	
	<!-- Stylesheets -->
	
	<link href="<?= base_url('assets/template/d/') ?>common-css/bootstrap.css" rel="stylesheet">
	
	
	<link href="<?= base_url('assets/template/d/') ?>common-css/fluidbox.min.css" rel="stylesheet">
	
	<link href="<?= base_url('assets/template/d/') ?>common-css/font-icon.css" rel="stylesheet">
	
	
	<link href="<?= base_url('assets/template/d/') ?>01-homepage/css/styles.css" rel="stylesheet">
	
	<link href="<?= base_url('assets/template/d/') ?>01-homepage/css/responsive.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Tajawal:400,500,700&display=swap&subset=arabic" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">

	<style>

		img.img-pengantin {
			width: 100%;
			border-radius: 50%;
		}

		.card-nama-mm {
			margin-top: -70px;
		}

		.card-nama-mm .card {
			padding: 10px 20px 10px 20px;
			border-radius: 20px;
		}

		.card-nama-mm .card p {
			font-size: 11pt;
			font-weight: 400;
		}

		.card-resepsi {
			padding: 20px;
			border-radius: 20px;
		}
        .main-slider {
            z-index: auto !important;
            color: #363636 !important;
        }

		@media screen and (max-width: 480px) {
			.card-nama-mm {
				margin-top: 10px;
			}

			.card-nama-mm .card {
				text-align: center !important;
			}
			.card-nama-mm .card p {
				font-size: 10pt;
			}
		}

		@media screen and (max-width: 480px) {
			.mobile-bottom-bar {
				width: 100%;
				position: fixed;
				bottom: 0;
				z-index: 9999;
				padding: 5px;
				background: #fff;
				border-top: solid 1px #cfcfcf;
				display: -webkit-flex;
				display: -ms-flexbox;
				display: flex;
				-webkit-justify-content: center;
					-ms-flex-pack: center;
						justify-content: center;
				-webkit-align-items: center;
					-ms-flex-align: center;
						align-items: center;
			}

			.mobile-bottom-bar .footer-link {
				-webkit-flex: 1 1 auto;
					-ms-flex: 1 1 auto;
						flex: 1 1 auto;
				text-align: center;
				color: #3d3d3d;
				text-transform: uppercase;
				font-size: 1.5rem;
				font-weight: bold;
				padding: 0;
				color:rgba(245, 28, 122, 0.8) !important;
			}

			.mobile-bottom-bar .footer-link i.fa {
				opacity: 0.8;
				margin-right: 0.625rem;
				font-size: 1.5rem;
				vertical-align: middle;
			}

			.mobile-bottom-bar .footer-link:focus, .mobile-bottom-bar .footer-link:active {
				color: #3d3d3d;
			}

			.mobile-bottom-bar .footer-text {
				position: relative;
				font-weight: bold;
				font-size: 8pt;
				color: #3d3d3d;
				margin-top: 0px;
				margin-bottom: 0 !important;
			}
			.banner_content img {
				display: block !important;
				width: 100%;
			}
			.banner_content h3 {
				font-size: 28pt !important;
			}
		}
	</style>
</head>
<body>
	
	
	<div class="main-slider" id="home" style="background-image: url(<?= base_url('assets/template/b/img/bg5.jpg') ?>); background-size:cover; background-position: center;">
		<div class="overlay"></div>
		<div class="display-table center-text">
			<div class="display-table-cell">
				<div class="slider-content">
					<i class="small-icon icon icon-tie"></i>
					<h4 class="pre-title">Wedding Invitation</h4>
					<h1 class="title"> <?= $wedding['nickname_man'] ?><i class="icon icon-heart"></i> <?= $wedding['nickname_woman'] ?></h1>
					<h5 class="date"><?= date('d F Y', strtotime($wedding['tanggal_resepsi'])).', '.$wedding['kota'] ?></h5>
                </div><!-- slider-content-->
			</div><!--display-table-cell-->
		</div><!-- display-table-->
	</div><!-- main-slider -->
	
	
	<section class="section story-area center-text" id="mempelai">
		<div class="container">
			<div class="row">
				<div class="col-md-6 mx-auto">
					
					<div class="row">
						<div class="col-md-6 col-sm-6 col-6">
							<img src="<?= $wedding['foto_pria'] ?>" alt="" class="img-pengantin">
						</div>
						<div class="col-md-6 col-sm-6 col-6">
							<img src="<?= $wedding['foto_wanita'] ?>" alt="" class="img-pengantin">
						</div>
					</div>
					
				</div>
				<div class="col-md-8 mx-auto">
					<div class="row">
						<div class="col-md-6 card-nama-mm">
							<div class="card card-body text-right">
								<h4><?= $wedding['fullname_man'] ?></h4>
								<p><?= $wedding['desc_man'] ?></p>
							</div>
						</div>
						<div class="col-md-6 card-nama-mm">
							<div class="card card-body text-left">
								<h4><?= $wedding['fullname_woman'] ?></h4>
								<p><?= $wedding['desc_woman'] ?></p>
							</div>
						</div>
					</div>
				</div>
			</div><!-- row -->
		</div><!-- container -->
    </section><!-- section -->
    
	<section class="section w-details-area center-text" id="acara" style="background-image: url(<?= base_url('assets/template/c/images/bg6.JPG') ?>); background-size:cover; background-position: top center;">
		<div class="container">
			<div class="row">
				<div class="col-sm-1"></div>
				<div class="col-sm-10">
					
					<div class="heading">
						<div class="text-center">
							<?php if($wedding['withBasmallah'] == 1){ ?>
								<h4 style="font-family: 'Tajawal', sans-serif;">بِسْمِ اللَّهِ الرَّحْمَنِ الرَّحِيم</h4>
								<h2>Assalamu'alaikum</h2>
							<?php } else {  ?>
								<h2>You are invited</h2>
							<?php } ?>
							<p>We invited you to celebrate our wedding</p>
							<br>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-6">
							<div class="card card-resepsi card-body">
								<h3>Akad Nikah</h3>
								<br>
								<h5><?= date('l', strtotime($wedding['tanggal_akad_nikah'])); ?>, <?= date('d M Y', strtotime($wedding['tanggal_akad_nikah'])); ?> <br><?= $wedding['jam_akad_nikah'] ?></h5>
								<hr>
								<h5><?= $wedding['lokasi_akad_nikah'] ?></h5>
								<p><?= $wedding['alamat_akad_nikah'] ?></p>
								<br>
        						<div class="date">
        							<a href="<?= $wedding['maps_akad_nikah'] ?>" class="btn btn-primary" target="_blank" >Lihat Maps</a>
        						</div>
							</div>
						</div>

						<div class="col-md-6">
							<div class="card card-resepsi card-body">
								<h3>Resepsi</h3>
								<br>
								<h5><?= date('l', strtotime($wedding['tanggal_resepsi'])); ?>, <?= date('d M Y', strtotime($wedding['tanggal_resepsi'])); ?> <br><?= $wedding['jam_resepsi'] ?></h5>
								<hr>
								<h5><?= $wedding['lokasi_resepsi'] ?></h5>
								<p><?= $wedding['alamat_resepsi'] ?></p>
								<br>
        						<div class="date">
        							<a href="<?= $wedding['maps_resepsi'] ?>" class="btn btn-primary" target="_blank" >Lihat Maps</a>
        						</div>
							</div>
						</div>
					</div>
					
				</div><!-- col-sm-10 -->
			</div><!-- row -->
		</div><!-- container -->
	</section><!-- section -->
	
	<section class="section galery-area center-text" id="gallery">
		<div class="container">
			<?php if($wedding['withGaleri'] == 1){ ?>
			<div class="row">
				
				<div class="col-sm-12">
					
					<div class="heading">
						<h2 class="title">Our Gallery</h2>
						<span class="heading-bottom"><i class="icon icon-star"></i></span>
					</div>
					
					<div class="image-gallery">
						<div class="row">
							<?php 
							$get_img = $this->db->get_where('invitation_gallery',array('invitation_id' => $wedding['id']));
							$count = $get_img->num_rows();
							foreach($get_img->result() as $i => $img): ?>
							<div class="col-md-4 col-sm-6">
								<a href="<?= $img->filename ?>" data-fluidbox><img class="margin-bottom" src="<?= $img->filename ?>" alt="Gallery Image"></a>
							</div><!-- col-sm-4 -->
							<?php endforeach; ?>
						</div><!-- row -->
						
					</div><!-- image-gallery -->
					
				</div><!-- col-sm-10 -->
			</div><!-- row -->
			<?php } ?>
		</div><!-- container -->
	</section><!-- section -->
	
	<footer>
		<div class="container center-text">
			
			<p class="copyright"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> Akadin.ID</p>
			
		</div><!-- container -->
	</footer>
	
	<div class="mobile-bottom-bar">
		<a href="#home" class="footer-link">
			<i class="fas fa-home"></i> 
			<p class='footer-text'>Home</p>
		</a>
		<a href="#mempelai" class="footer-link">
			<i class="fas fa-heart"></i>
			<p class='footer-text'>Mempelai</p>
		</a>
		<a href="#acara" class="footer-link">
			<i class="far fa-calendar-alt"></i>
			<p class='footer-text'>Acara</p>
		</a>
		<a href="#gallery" class="footer-link">
			<i class="far fa-images"></i>
			<p class='footer-text'>Gallery</p>
		</a>
	</div>
	
	<!-- SCIPTS -->
	
	<script src="<?= base_url('assets/template/d/') ?>common-js/jquery-3.1.1.min.js"></script>
	
	<script src="<?= base_url('assets/template/d/') ?>common-js/tether.min.js"></script>
	
	<script src="<?= base_url('assets/template/d/') ?>common-js/bootstrap.js"></script>
	
	<script src="<?= base_url('assets/template/d/') ?>common-js/jquery.countdown.min.js"></script>
	
	<script src="<?= base_url('assets/template/d/') ?>common-js/jquery.fluidbox.min.js"></script>
	
	
	<script src="<?= base_url('assets/template/d/') ?>common-js/scripts.js"></script>

	<script>
		var cw = $('img.img-pengantin').width();
		$('img.img-pengantin').css({'height':cw+'px'});
		
		if(isExists('#clock')){
			$('#clock').countdown("<?= date('Y/m/d', strtotime($wedding['tanggal_resepsi'])) ?>", function(event){
				var $this = $(this).html(event.strftime(''
					+ '<div class="time-sec"><span class="title">%D</span> days </div>'
					+ '<div class="time-sec"><span class="title">%H</span> hours </div>'
					+ '<div class="time-sec"><span class="title">%M</span> minutes </div>'
					+ '<div class="time-sec"><span class="title">%S</span> seconds </div>'));
			});
		}
	</script>
	
</body>
</html>