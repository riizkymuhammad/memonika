<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>  
  </nav>
  <!-- /.navbar -->
<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="" class="brand-link">
    <span class="brand-text font-weight-light">Admin Akadin.ID </span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="info">
        <a href="#" class="d-block">Hai</a>
        </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
            with font-awesome or any other icon font library -->
        <li class="nav-item">
            <a href="{{ route('dashboard') }}" class="nav-link" >
            <i class="far fa-circle nav-icon"></i>
            <p>Dashboard</p>
            </a>
        </li>
        <li class="nav-header">e-Invitations</li>
        <li class="nav-item">
            <a href="{{ route('addInv') }}" class="nav-link ">
            <i class="nav-icon fas fa-plus"></i>
            <p>
                Tambah
            </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('listInv') }}" class="nav-link " >
            <i class="nav-icon far fa-envelope"></i>
            <p>
                List Undangan
            </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('listTemplate') }}" class="nav-link ">
            <i class="nav-icon fas fa-images"></i>
            <p>
                List Template
            </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('listMusic') }}" class="nav-link ">
            <i class="nav-icon fas fa-music"></i>
            <p>
                List Musik
            </p>
            </a>
        </li>
        <li class="nav-header">MANAJEMEN PROFIL</li>
        <li class="nav-item">
            <a href="" class="nav-link ">
            <i class="nav-icon fas fa-user"></i>
            <p>
                Edit Profil dan Password
            </p>
            </a>
        </li>
        <li class="nav-header">-</li>
        <li class="nav-item">
            <a href="" class="nav-link">
            <i class="fas fa-sign-out-alt"></i>
            <p>
                Keluar
            </p>
            </a>
        </li>
        </ul>
    </nav>
    <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>