<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class InvitationTemplates extends Model
{
    protected $table = 'invitation_templates';

    protected $guarded = [];
}
