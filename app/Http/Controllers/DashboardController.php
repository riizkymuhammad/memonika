<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index()
	{
        // $this->db->where('status !=', '9');
        $data['invitation'] = DB::table('invitations')->get();
        $data['template'] = DB::table('invitation_templates')->get();
        // $data['template'] = $this->db->get('invitation_templates')->result();
        return view('dashbor',$data);
		// $this->load->view('dasbor',$data);
    }

    public function addInv()
    {
        $data['wedding'] = null;
        return view('addInv',$data);
    }

    public function listInv()
    {
        if(isset($_POST['id'])) {
            $this->chageStatus();
        }
        // $this->db->where('status !=', '9');
        $data['invitation'] = DB::table('invitations')->get();
        // $data['invitation'] = $this->db->get('invitations');
        return view('listInv',$data);
        // $this->load->view('listInv',$data);
    }

    public function EditInv($id)
    {
        $data['wedding'] = DB::table('invitations')
        ->join('invitation_contents','invitation_contents.invitation_id','invitations.id')
        ->where('invitations.id',$id)->get();
        // $data['invitation'] = $this->db->get('invitations');
        return view('editInv',$data);
    }

    public function listTemplate()
    {
        $data['template'] = DB::table('invitation_templates')->get();
        // $data['template'] = $this->db->get('invitation_templates')->result();
        return view('listTemplate',$data);
        // $this->load->view('listTemplate',$data);
    }

    public function listMusic()
    {
        // $data['music'] = $this->db->get('music_list')->result();
        $data['music'] = DB::table('music_list')->get();
        return view('listMusic',$data);
        // $this->load->view('list_music',$data);
    }

}
