<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InvitationController extends Controller
{
    public function example($filename)
	{
    $data['wedding'] =  DB::table('invitations')
    ->join('invitation_contents','invitation_contents.invitation_id','invitations.id')
    ->where('slug','example')
    ->first();

    $data['lagu'] =  $data['wedding']->lagu;

    $data['comment'] =  DB::table('invitation_comments')
    ->orderBy('id','DESC')
    ->get()
    ->toArray();

    $get_img =  DB::table('invitation_gallery')
    ->where('invitation_id',$data['wedding']->id)->get()
    ->toArray();

    
    return view('templates/'.$filename,compact('data','get_img'));
    // $this->db->select("*");
    // $this->db->from("invitations");
    // $this->db->join("invitation_contents","invitation_contents.invitation_id = invitations.id");
    // $this->db->where("slug","example");
    // $wedding = $this->db->get()->row_array();
    // $data['wedding'] = $wedding;
    // $this->load->view('templates/'.$this->uri->segment(2),$data);
}
}
