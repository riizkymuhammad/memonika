<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InvitationTemplates;
use App\Invitation;

class HomepageController extends Controller
{
    public function index()
    {
        $data['invitation'] = InvitationTemplates::all();
        return view('welcome',$data);
    }

    public function create()
    {
        $data['invitation'] = InvitationTemplates::all();
        return view('front.create',$data);
    }

    public function create_addon()
    {
        return view('front.create_addon');
    }

    public function createAddon()
    {
        $data['invitation'] = InvitationTemplates::all();
        return view('front.createAddon',$data);
    }

    public function createData()
    {
        $data['invitation'] = InvitationTemplates::all();
        $data['wedding']= Invitation::select('*')
        ->join('invitation_contents','invitation_contents.invitation_id','invitations.id')
        ->get();
        return view('front.createData',$data);
    }

    public function createPay()
    {
        return view('front.createPay');
    }

    public function memonika()
    {
        return view('memonika');
    }

    public function faq()
    {
        return view('faq');
    }

    public function panduanpengguna()
    {
        return view('panduanpengguna');
    }

    public function privacypolicy()
    {
        return view('privacypolicy');
    }

}
