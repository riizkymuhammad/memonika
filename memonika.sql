-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 14, 2020 at 09:11 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `memonika`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `email` varchar(55) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invitations`
--

CREATE TABLE `invitations` (
  `id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `slug` varchar(55) NOT NULL,
  `template` varchar(15) NOT NULL,
  `date_created` varchar(55) NOT NULL,
  `status` int(1) NOT NULL,
  `ket` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invitations`
--

INSERT INTO `invitations` (`id`, `name`, `slug`, `template`, `date_created`, `status`, `ket`) VALUES
(10, 'Wildan Nissa', 'example', 'a4', '1571788814', 9, 'ini adalah data contoh'),
(14, 'Wynter Stafford', 'asdfasdf', 'a2', '1572750722', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `invitation_comments`
--

CREATE TABLE `invitation_comments` (
  `id` int(11) NOT NULL,
  `invitation_id` int(11) NOT NULL,
  `is_attend` varchar(15) NOT NULL,
  `fullname` varchar(55) NOT NULL,
  `comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invitation_comments`
--

INSERT INTO `invitation_comments` (`id`, `invitation_id`, `is_attend`, `fullname`, `comment`) VALUES
(1, 10, '3', 'Jescie Evans', ' asdfasdf'),
(2, 10, '3', 'Jescie Evans', ' asdfasdfsdfas'),
(3, 10, '2', 'Wylie Pace', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'),
(4, 10, '1', 'Geoffrey William', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.');

-- --------------------------------------------------------

--
-- Table structure for table `invitation_contents`
--

CREATE TABLE `invitation_contents` (
  `id` int(11) NOT NULL,
  `invitation_id` int(11) NOT NULL,
  `nickname_man` varchar(55) NOT NULL,
  `nickname_woman` varchar(55) NOT NULL,
  `fullname_man` varchar(55) NOT NULL,
  `fullname_woman` varchar(55) NOT NULL,
  `desc_man` varchar(255) NOT NULL,
  `desc_woman` varchar(255) NOT NULL,
  `kota` varchar(55) NOT NULL,
  `withAkad` int(1) NOT NULL,
  `withBasmallah` int(1) NOT NULL,
  `tanggal_akad_nikah` varchar(55) NOT NULL,
  `jam_akad_nikah` varchar(55) NOT NULL,
  `maps_akad_nikah` varchar(255) NOT NULL,
  `lokasi_akad_nikah` varchar(155) NOT NULL,
  `alamat_akad_nikah` varchar(255) NOT NULL,
  `tanggal_resepsi` varchar(55) NOT NULL,
  `jam_resepsi` varchar(55) NOT NULL,
  `maps_resepsi` varchar(255) NOT NULL,
  `lokasi_resepsi` varchar(155) NOT NULL,
  `alamat_resepsi` varchar(255) NOT NULL,
  `foto_sampul` varchar(255) NOT NULL,
  `foto_pria` varchar(255) NOT NULL,
  `foto_wanita` varchar(255) NOT NULL,
  `lagu` varchar(255) NOT NULL,
  `withGaleri` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invitation_contents`
--

INSERT INTO `invitation_contents` (`id`, `invitation_id`, `nickname_man`, `nickname_woman`, `fullname_man`, `fullname_woman`, `desc_man`, `desc_woman`, `kota`, `withAkad`, `withBasmallah`, `tanggal_akad_nikah`, `jam_akad_nikah`, `maps_akad_nikah`, `lokasi_akad_nikah`, `alamat_akad_nikah`, `tanggal_resepsi`, `jam_resepsi`, `maps_resepsi`, `lokasi_resepsi`, `alamat_resepsi`, `foto_sampul`, `foto_pria`, `foto_wanita`, `lagu`, `withGaleri`) VALUES
(10, 10, 'Rahmat', 'Nissa', 'Rahmat Maulana', 'Annisa', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', 'Yogyakarta', 1, 1, '2020-02-20', '08.00 - 09.00', 'https://goo.gl/maps/eokWRn8RxCvGY1Ae9', 'Masjid Jogokariyan', 'Jl. Jogokaryan No.36, Mantrijeron, Kec. Mantrijeron, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55143', '2020-02-20', '10.00 - 11.00', 'https://goo.gl/maps/eokWRn8RxCvGY1Ae9', 'Masjid Jogokariyan', 'Jl. Jogokaryan No.36, Mantrijeron, Kec. Mantrijeron, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55143', 'http://localhost/zuwaja/assets/uploads/845adaf08a9d5ebe8a4bc4110e67def6.jpg', 'http://localhost/zuwaja/assets/uploads/me_wildan.JPG', 'http://localhost/zuwaja/assets/uploads/muslimah.png', 'http://localhost/zuwaja/assets/uploads/06__Seperti_Yang_Kau_Minta_-_MP3_Download,_Play,_Listen_Songs_-_4shared.mp3', 1);

-- --------------------------------------------------------

--
-- Table structure for table `invitation_gallery`
--

CREATE TABLE `invitation_gallery` (
  `id` int(11) NOT NULL,
  `invitation_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invitation_gallery`
--

INSERT INTO `invitation_gallery` (`id`, `invitation_id`, `filename`) VALUES
(20, 10, 'http://localhost/akadin/assets/uploads/2522535.jpg'),
(21, 10, 'http://localhost/akadin/assets/uploads/Sisa-Erupsi-Merapi-Yang-Menjadi-Lava-Tour.jpg'),
(22, 10, 'http://localhost/akadin/assets/uploads/candi-prambanan-pusat-hari-nyepi-di-dunia-JKouHUcDWX.jpg'),
(23, 10, 'http://localhost/akadin/assets/uploads/delokal-bukit-paralayang-jogja.jpg'),
(24, 10, 'http://localhost/akadin/assets/uploads/Harga-Tiket-Masuk-Taman-Sari-Jogja.jpg'),
(25, 10, 'http://localhost/akadin/assets/uploads/20507583_1531555953568044_162724143130407848_o.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `invitation_templates`
--

CREATE TABLE `invitation_templates` (
  `id` int(11) NOT NULL,
  `filename` varchar(55) NOT NULL,
  `ui_desktop` varchar(255) NOT NULL,
  `ui_mobile` varchar(255) NOT NULL,
  `price` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invitation_templates`
--

INSERT INTO `invitation_templates` (`id`, `filename`, `ui_desktop`, `ui_mobile`, `price`) VALUES
(8, 'a1', 'http://localhost/memonika/public/assets/uploads/A11.jpg', '', 50000),
(9, 'a2', 'http://localhost/memonika/public/assets/uploads/A2.jpg', '', 50000),
(10, 'a3', 'http://localhost/memonika/public/assets/uploads/a3.jpg', '', 50000);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `music_list`
--

CREATE TABLE `music_list` (
  `id` int(11) NOT NULL,
  `filename` varchar(155) NOT NULL,
  `url` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `music_list`
--

INSERT INTO `music_list` (`id`, `filename`, `url`, `date_created`) VALUES
(7, 'ipang_-_sahabat_kecil.mp3', 'http://localhost/akadin/assets/uploads/ipang_-_sahabat_kecil.mp3', '2019-11-03 03:02:35'),
(8, '06__Seperti_Yang_Kau_Minta_-_MP3_Download,_Play,_Listen_Songs_-_4shared.mp3', 'http://localhost/akadin/assets/uploads/06__Seperti_Yang_Kau_Minta_-_MP3_Download,_Play,_Listen_Songs_-_4shared.mp3', '2019-11-03 03:06:49');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pricing`
--

CREATE TABLE `pricing` (
  `id` int(11) NOT NULL,
  `ytb` int(15) NOT NULL,
  `comm` int(15) NOT NULL,
  `fund` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pricing`
--

INSERT INTO `pricing` (`id`, `ytb`, `comm`, `fund`) VALUES
(1, 55000, 80000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@mail.com', NULL, '$2y$10$jB8T5GwN2yCmE4pcNvQ/3ujDdS4b8pSN0c9zzcghiO3MiGeFx9atW', NULL, '2020-03-13 07:57:27', '2020-03-13 07:57:27');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
